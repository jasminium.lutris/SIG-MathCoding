%
% This is a different version of formalizing the rules in the DCG
% This dictionary specifies the rules in a way that makes it possible
% to call a bottom-up parser to derive the structure of the sentence.
%

% This rule allows us to use ---> instead of --> to define our CFG.
:- op(700,xfx, --->).

% Phrase Structure Rules
s     ---> [np, vp].
np    ---> [pn].
np    ---> [pro].
np    ---> [pn,rel].
np    ---> [det, nbar].
nbar  ---> [n].
nbar  ---> [n, rel].
rel   ---> [wh, vp].
vp    ---> [iv].
vp    ---> [tv, np].
vp    ---> [dv, np, pp].
vp    ---> [dv, np, np].
vp    ---> [sv, s].
pp    ---> [p, np].

% Proper Nouns
lex(vincent,pn).
lex(mia,pn).
lex(marsellus,pn).
lex(jules,pn).

% Pronouns
lex(i, pro).
lex(you, pro).
lex(he, pro).
lex(she, pro).
lex(it, pro).
lex(we, pro).
lex(they, pro).
lex(me, pro).
lex(him, pro).
lex(her, pro).
lex(us, pro).
lex(them, pro).

% Determiners
lex(a,det).
lex(the,det).
lex(her,det).
lex(his,det).
lex(my, det).
lex(your, det).
lex(our, det).
lex(its, det).
lex(their, det).
lex(some, det).
lex(many,det).
lex(any, det).

% Nouns
lex(gun,n).
lex(robber,n).
lex(man,n).
lex(woman,n).
lex(student, n).
lex(wizard, n).
lex(witch, n).
lex(bread, n).
lex(soup, n).
lex(game, n).

% relative pronouns
lex(who,wh).
lex(that,wh).

% Prepositions
lex(to,p).
lex(at, p).
lex(in, p).
lex(on, p).
lex(with, p).
lex(for, p).

% Intransitive Verbs
lex(died,iv).
lex(fell,iv).

% Transitive Verbs
lex(loved,tv).
lex(shot,tv).
lex(knew,tv).

% Ditransitive Verbs
lex(gave,dv).
lex(handed,dv).
lex(sent, dv).
lex(taught, dv).

% Stative Verbs
lex(knew,sv).
lex(believed,sv).
lex(said, sv).
lex(thought, sv).
