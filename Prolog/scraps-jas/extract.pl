use_module(library(sgml)).

extract(EL, TYPE, ID, DATA) :-
	EL = element(TYPE, ID, DATA).

extract_list(LIST, TYPE, ID, DATA) :-
	maplist(extract, LIST, TYPE, ID, DATA).

load_verbs(FILE, ID, MEMBERS, THEMROLES, FRAMES, SUBCLASSES) :-
	load_xml(FILE, [element(_,ID,DATA)], _),
	DATA = [element('MEMBERS', _, MEMBER_DATA),
			element('THEMROLES', _, THEMROLES),
			element('FRAMES', _, FRAMES),
			element('SUBCLASSES', _, SUBCLASSES)],

	maplist(extract, MEMBER_DATA, _, MEMBERS, _).

parse_member(X, NAME, WN, GROUP) :-
	X = [name=NAME, wn=WN, grouping=GROUP].