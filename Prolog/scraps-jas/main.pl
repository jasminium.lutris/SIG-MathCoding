utterance(S, Graph) :-
    s(S, [ ], Graph).

s(St, Nd, Graph) :-
    np(St, X, GraphSubj),
    vp(X, Nd, GraphPred),
    join([agent(GraphSubj)], GraphPred, Graph).

np([N | Nd], Nd, GraphNP) :-
    n(N, GraphNP).

np([Det, N | Nd], Nd, GraphNP) :-
    det(Det),
    n(N, GraphNP).

vp([V | Nd], Nd, GraphVP) :-
    v(V, GraphVP).

vp([V | X], Nd, GraphVP) :-
    v(V, GraphV),
    np(X, Nd, GraphNP),
    join([object(GraphNP)], GraphV, GraphVP).


% data structure

join(X, X, X).
join(A, B, Ret) :-
    is_frame(A), is_frame(B), !,
    join_frames(A, B, Ret, not_ok).

join(A, B, Ret) :-
    is_frame(A), is_slot(B), !,
    join_slot_to_frame(B, A, Ret).
join(A, B, Ret) :-
    is_frame(B), is_slot(A), !,
    join_slot_to_frame(A, B, Ret).

join(A, B, Ret) :-
    is_slot(A), is_slot(B), !,
    join_slots(A, B, Ret).


is_frame([_|_]).
is_frame([]).

is_slot(A) :-
    functor(A, _, 1).

join_frames([A|B], C, D, _) :-
    join_slot_to_frame(A, C, E), !,
    join_frames(B, E, D, ok).
join_frames([A|B], C, [A|D], OK) :-
    join_frames(B, C, D, OK), !.
join_frames([], A, A, ok).

join_slot_to_frame(A, [B|Tail], [D|Tail]) :-
    join_slots(A, B, D).
join_slot_to_frame(A, [Head|C], [Head|D]) :-
    join_slot_to_frame(A, C, D).

% TODO: grok
join_slots(A, B, D) :-
    functor(A, FA, _), functor(B, FB, _),
    matches(FA, FB, FN),
    arg(1, A, Aval), arg(1, B, Bval),
    join(Aval, Bval, Retval),
    D =.. [FN | [Retval]].

% TODO: search over semantic properties of nouns
matches(X, X, X).
matches(Thing, Desc, Thing) :-
    matches(Thing, Desc).
matches(Desc, Thing, Thing) :-
    matches(Thing, Desc).


% data

% TODO: animate(owl), animate(ram)
matches(owl, animate).
matches(coat, equipment).

n(owl, [owl(X)]).
n(coat, [coat(X)]).

det(the).
det(a).

v(saw, [agent([animate(X)]) ]).
v(wore, [object([equipment(X)]) ]).
