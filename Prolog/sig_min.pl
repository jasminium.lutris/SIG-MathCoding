% to list one-by-one:
% s(X). ; ; ; ; % ...

% for graphical debugger:
% tspy(s). s(X).

% only verbs without object
% s(S), S = (_, VP), VP = (V), v(V).

s(S) :-
    S = (NP, VP),
    np(NP), vp(VP),

    VP = (V, NP),
    action(V).

s(S) :-
    S = (NP, VP),
    np(NP), vp(VP),

    VP = (V),
    \+ action(V).

np(NP) :-
    NP = (DET, N),
    det(DET), n(N).

vp(VP) :-
    VP = (V, NP),
    v(V), np(NP),
    action(V).

vp(VP) :-
    VP = (V),
    v(V),
    \+ action(V).

%    NP = (_, N),
%    nocturnal(N).

n(owl).
n(ram).
n(wall).

v(saw).
%v(feared).
%v(sought).
v(broke).

det(the).
det(a).

action(saw).

% nocturnal(owl).

/* TODO: pipe to file.
print([]).
print([H|T]) :-
    write(H),
    print(T).

print(all) :-
    findall(X, s(X), Out),
    print(Out).
*/
