# Log

*   **Week 1:** Warmups. Mentions of poetry, genomics, and graph theory.
*	**Week 3:** Plotting phaseplanes of dynamical systems with xarray.

## Agenda

Default: At individuals' discretion: Report project status, technical concerns, planned readings, etcetera.

*   **Week 4:** Scraping descriptions with beautifulsoup for analysis in theano.
*   **Week 5:** Genomics in R / bioreactor.

I am liable to assume basic knowledge of functional (higher-order) programming (e.g. Pythonic for comprehensions, R vectorized functions), but newcomers will be pointed in the right direction.

## Resources

[Transphoner](http://graphics.stanford.edu/projects/transphoner/), from Stanford.

### Current Literature

[arXiv](https://arxiv.org/), the preprint repository.

TTIC [Distinguished Lectures](http://www.ttic.edu/dls.php), as a general-audience resource regarding AI. Currently: optimistic planning for robots [(Kaelbling)](https://www.youtube.com/watch?v=HcfpHZJu32A), just-enough linguistic structure for comprehension [(Manning)](https://www.youtube.com/watch?v=8ndvZaFPl_4), learning which features are important before constructing your model [(Tenenbaum)](https://www.youtube.com/watch?v=Js0hXfA1SG4).

### Prolog - Unification

*poetry in ipa* - construct this dataset. visualize similarities.
- reference:
- http://graphics.stanford.edu/projects/transphoner/
- Kincaid reading level, iwl.me, polishmywriting.com, ...

Prof. Kauffman's [webpage](http://homepages.math.uic.edu/~kauffman/).

VerbNet, Perseus?

Representation and Inference for Natural Language (Blackburn & Bos), for semantic nets.

SWI-Prolog's [SWISH](http://swish.swi-prolog.org/#tabbed-tab-2), with [online companion book](http://www.learnprolognow.org/lpnpage.php?pageid=online).

### Java - Functional Types

Scala. Clojure?

[NetLogo](https://ccl.northwestern.edu/netlogo/) official site, with [online examples](http://www.netlogoweb.org/launch#http://www.netlogoweb.org/assets/modelslib/Sample%20Models/Biology/Rabbits%20Grass%20Weeds.nlogo).

### R - Statistics

*genomic data* - similarity metrics.
- reference: ongoing coursework

Quantopian?

### Numeric Python

*graph theory*
- reference: ?

### Symbolic Python

[SAGEMathCell](https://sagecell.sagemath.org/) for quick computations. [SAGEMathCloud](https://cloud.sagemath.com/) for full projects. Consult [main site](http://www.sagemath.org/) for local installs.

Prof. Verschelde's [MCS 320 course page](http://homepages.math.uic.edu/~jan/mcs320/), with [extensive notes](http://homepages.math.uic.edu/~jan/mcs320/mcs320notes/lec01.html) at the calculus level, with examples including BCH codes, four-body problems, and Rubik's cube.

Sage Wiki's [interactive examples](https://wiki.sagemath.org/interact).


Thanks to Marshall Hampton via SAGE Wiki for the [polar parametric curve code](https://wiki.sagemath.org/interact/calculus#Some_polar_parametric_curves) used for our current icon.