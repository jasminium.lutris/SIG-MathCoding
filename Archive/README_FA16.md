# Log

*   **Week 3:** Used SAGE to verify e^(ix) = cos(x) + i*sin(x), the sin series expansion of f(x) = x, and the solution of f'' + f = x.  
    Pinned time down to 1pm. Will henceforth meet in CS Lounge, on the couches, due to low volume.
*   **Week 5:** Discussed potential projects in R and Prolog.  
*   **Week 7:** Prolog: incorporate verbnet, generate sentence stories.  
    R: simulate interaction dynamics, create shared folder (~~Dropbox?~~).  
*   **Week 8:** Introduced SAGE (ex: rose curves) and Prolog (ex: formal grammar) to new students.  
    Indicated further online resources for R and NetLogo.
*   **Week 9:** Split Prolog project into sentence parser and VerbNet interface. Discussed minimalistic grammars. Viewed Perseus digital library.
*   **Week 10:** XML parsing in Prolog, diffusion over graphs, cliodynamics.
*   **Week 11:** Abstract discussion of parameter sensitivity analysis. Small demo of symmetric groups. (Comparison of parsing strategies.)
*   **Week 12:** FOXP genes, in the context of language production. Gap and Hox genes, in the context of the Geirer-Meinhardt model. Clean energy stocks on Quantopian.
*   **Week 13:** Chart parsing, mean reversion, quantifiers.

## Agenda

Default: At individuals' discretion: Report project status, technical concerns, planned readings, etcetera.

*   **Week 3:** using power series to compute cos + i sin (i.e. Euler's formula); investigate SAGE interacts  
    ~~functional vs. logic programming~~  
*   **Week 4:** off  
*   **Week 5:** interactive R plots via SageMathCell and Jupyter; investigate R + D3  
    ~~introduce logic programming, optimization; investigate TBD~~  
*   **Week 6:** off  
*   **Week 7:** Scope projects: currently, these are analysis of a complex dynamical system, and semantic awareness. 
*   **Week 9:** Follow up on projects. Discuss new members' interests and scope their projects.
*   ...
*   **Week 14:** off
*   **Week 15:** Scope out next semester.

## Targets

**SAGEMath:** symbolic calculus & group theory, interactive plots  
**VerbNet:** pragmatic knowledge for syntactic agreement

**Transcendental Functions** (the precise relationship of exp, cos, sin)  
**Dynamical Systems** (population dynamics, etc.)

...and more besides, as per the log.

I am liable to assume basic knowledge of functional (higher-order) programming (e.g. Pythonic for comprehensions, R vectorized functions), but newcomers will be pointed in the right direction.

## Resources

### Current Literature

TTIC [Distinguished Lectures](http://www.ttic.edu/dls.php), as a general-audience resource regarding AI. Currently: optimistic planning for robots [(Kaelbling)](https://www.youtube.com/watch?v=HcfpHZJu32A), just-enough linguistic structure for comprehension [(Manning)](https://www.youtube.com/watch?v=8ndvZaFPl_4), learning which features are important before constructing your model [(Tenenbaum)](https://www.youtube.com/watch?v=Js0hXfA1SG4).

### Prolog

Representation and Inference for Natural Language (Blackburn & Bos), for semantic nets.

SWI-Prolog's [SWISH](http://swish.swi-prolog.org/#tabbed-tab-2), with [online companion book](http://www.learnprolognow.org/lpnpage.php?pageid=online).

### Java Ecosystem

Scala, for functional programming. Clojure, too?

[NetLogo](https://ccl.northwestern.edu/netlogo/) official site, with [online examples](http://www.netlogoweb.org/launch#http://www.netlogoweb.org/assets/modelslib/Sample%20Models/Biology/Rabbits%20Grass%20Weeds.nlogo).

### Data

[VerbNet. Perseus. Quantopian.]

### SAGEMath (IPython)

[SAGEMathCell](https://sagecell.sagemath.org/) for quick computations. [SAGEMathCloud](https://cloud.sagemath.com/) for full projects.

Consult [main site](http://www.sagemath.org/) for local installs, which we will cover the first week.

Prof. Verschelde's [MCS 320 course page](http://homepages.math.uic.edu/~jan/mcs320/), with [extensive notes](http://homepages.math.uic.edu/~jan/mcs320/mcs320notes/lec01.html) we will reference.

Sage Wiki's [interactive examples](https://wiki.sagemath.org/interact). ~~Find [working](https://wiki.sagemath.org/interact/calculus?action=recall&rev=51) [code](https://wiki.sagemath.org/interact/number_theory?action=recall&rev=13) in pages' revision history (under 'Info').~~

### Models?

Prof. Kauffman's [webpage](http://homepages.math.uic.edu/~kauffman/).

## Backburner

**matplotlib:** parametric and implicit plots & animations  
**ipython / D3:** browser-based interactive widgets

**Laplace Transform** (decompose analytic functions into growth & periodic components)  
**Matrix Decomposition** (automate solution of Ax = b by Gaussian elimination)  
**Linear Optimization** (maximize utility under constraints)

...and the rigorous basis of other common engineering techniques. The tools are also good for number theory, game theory, graph theory, optimization, and other cool things we may explore.

Thanks to Marshall Hampton via SAGE Wiki for the [polar parametric curve code](https://wiki.sagemath.org/interact/calculus#Some_polar_parametric_curves) used for our current icon.</div>